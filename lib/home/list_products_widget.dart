import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/blocs/carts_bloc.dart';
import 'package:shopping_cart/class/data_base.dart';
import 'package:shopping_cart/class/decoration_widget.dart';
import 'package:shopping_cart/controller/carts.dart';
import 'package:shopping_cart/controller/product.dart';
import 'package:shopping_cart/data_model/products.dart';

class ListProductsWidget extends StatelessWidget with DataBase {
  ListProductsWidget({Key? key}) : super(key: key);

  final CartsController cartsController = Get.put(CartsController());
  final ProductController productController = Get.put(ProductController());
  final DecorationWidget decorationWidget = DecorationWidget();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Shopping Cart Test'),
        backgroundColor: Colors.grey,
        actions: [
          BlocBuilder<CartsBloc, CartsState>(
            builder: (context, state) {
              return AnimatedCrossFade(
                duration: const Duration(milliseconds: 500),
                firstCurve: Curves.easeInOutCubic,
                crossFadeState: !state.showCarts!
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                firstChild: Center(
                  child: TextButton.icon(
                    //onPressed: () => cartsController.showCartLayout(),
                      onPressed: () =>
                          BlocProvider.of<CartsBloc>(context).add(
                              ShowCartLayout()),
                      icon: const Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                      ),
                      label: const Text(
                        'Ver Carrito',
                        style: TextStyle(color: Colors.white),
                      )),
                ),
                secondChild: Center(
                  child: TextButton.icon(
                      onPressed: null,
                      icon: const Icon(
                        Icons.shopping_cart,
                        color: Colors.transparent,
                      ),
                      label: const Text('Ver carrito',
                          style: TextStyle(color: Colors.transparent))),
                ),
              );
            },
          )
        ],
      ),
      body: Container(
        margin: decorationWidget.marginContainerPage(),
        decoration: decorationWidget.containerDecorationPage(
          color: Colors.grey,
        ),
        child: StreamBuilder<List<Products>>(
            stream: getDataProductsList(),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(snapshot.error.toString()),
                );
              }
              if (snapshot.hasData) {
                return ListView.separated(
                    itemBuilder: (context, index) {
                      Products productsInfo = snapshot.data![index];
                      return ListTile(
                          title: Text(productsInfo.nombre!),
                          subtitle: Text(productsInfo.descripcion!),
                          trailing: BlocBuilder<CartsBloc, CartsState>(
                            builder: (context, state) {
                              return AnimatedCrossFade(
                                  alignment: Alignment.center,
                                  firstCurve: Curves.easeInOutCubic,
                                  secondCurve: Curves.easeInOutCubic,
                                  sizeCurve: Curves.easeInOutCubic,
                                  firstChild: IconButton(
                                    onPressed: () {
                                      Map<String, dynamic> data =
                                      productsInfo.toJson();
                                      data.putIfAbsent('quantity', () => 1);
                                      addProductIntoCart(
                                          idCart:
                                          state.nameCart!,
                                          data: data)
                                          .then((value) =>
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(SnackBar(
                                            content: Text(value),
                                          )))
                                          .catchError((error) {
                                        print(error.toString());
                                      });
                                    },
                                    icon: const Icon(Icons.shopping_cart),
                                  ),
                                  secondChild: const IconButton(
                                    onPressed: null,
                                    icon: Icon(
                                      Icons.star,
                                      color: Colors.transparent,
                                    ),
                                  ),
                                  crossFadeState:
                                  state.showProductsIntoCart!
                                      ? CrossFadeState.showFirst
                                      : CrossFadeState.showSecond,
                                  duration: const Duration(
                                      milliseconds: 1000));
                            },
                          ));
                    },
                    separatorBuilder: (context, index) {
                      return const Divider();
                    },
                    itemCount: snapshot.data!.length);
              } else {
                return const Center(child: CircularProgressIndicator());
              }
            }),
      ),
    );
  }
}

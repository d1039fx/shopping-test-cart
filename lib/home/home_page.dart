import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/blocs/carts_bloc.dart';
import 'package:shopping_cart/carts/cart_pages.dart';
import 'package:shopping_cart/carts/products_into_cart_page.dart';
import 'package:shopping_cart/controller/carts.dart';

import 'package:shopping_cart/home/list_products_widget.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final CartsController cartsController = Get.put(CartsController());

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //products
        BlocBuilder<CartsBloc, CartsState>(
          builder: (context, state) {
            return AnimatedPositioned(
                left: 0,
                width: state.showCarts!
                    ? MediaQuery.of(context).size.width / 2
                    : MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                curve: Curves.easeInOutCubic,
                duration: const Duration(milliseconds: 500),
                child: ListProductsWidget());
          },
        ),
        // Obx(() {
        //   return AnimatedPositioned(
        //       left: 0,
        //       width: cartsController.showCarts.value
        //           ? MediaQuery.of(context).size.width / 2
        //           : MediaQuery.of(context).size.width,
        //       height: MediaQuery.of(context).size.height,
        //       curve: Curves.easeInOutCubic,
        //       duration: const Duration(milliseconds: 500),
        //       child: ListProductsWidget());
        // }),
        //cart
        BlocBuilder<CartsBloc, CartsState>(
          builder: (context, state) {
            return AnimatedPositioned(
              left: state.showCarts!
                  ? MediaQuery.of(context).size.width / 2
                  : MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width / 2,
              curve: Curves.easeInOutCubic,
              duration: const Duration(milliseconds: 500),
              child: CartsPage(),
            );
          },
        ),
        // Obx(() {
        //   return AnimatedPositioned(
        //     left: cartsController.showCarts.value
        //         ? MediaQuery.of(context).size.width / 2
        //         : MediaQuery.of(context).size.width,
        //     height: MediaQuery.of(context).size.height,
        //     width: MediaQuery.of(context).size.width / 2,
        //     curve: Curves.easeInOutCubic,
        //     duration: const Duration(milliseconds: 500),
        //     child: CartsPage(),
        //   );
        // }),

        //products into carts
        BlocBuilder<CartsBloc, CartsState>(
  builder: (context, state) {
    return AnimatedPositioned(
          top: !state.showProductsIntoCart!
              ? MediaQuery.of(context).size.height
              : 0,
          right: 0,
          curve: Curves.easeInOutCubic,
          duration: const Duration(milliseconds: 500),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width / 2,
          child: ProductsIntoCartPage(),
        );
  },
)
        // Obx(() {
        //   return AnimatedPositioned(
        //     top: !cartsController.showProductsIntoCart.value
        //         ? MediaQuery.of(context).size.height
        //         : 0,
        //     right: 0,
        //     curve: Curves.easeInOutCubic,
        //     duration: const Duration(milliseconds: 500),
        //     height: MediaQuery.of(context).size.height,
        //     width: MediaQuery.of(context).size.width / 2,
        //     child: ProductsIntoCartPage(),
        //   );
        // }),
      ],
    );
  }
}

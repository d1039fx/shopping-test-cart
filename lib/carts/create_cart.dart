import 'package:flutter/material.dart';
import 'package:shopping_cart/class/data_base.dart';
import 'package:shopping_cart/data_model/carts.dart';

class CreateCart extends StatelessWidget with DataBase {
  CreateCart({Key? key}) : super(key: key);

  final TextEditingController nameCart = TextEditingController();
  final GlobalKey<FormState> form = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
        key: form,
        child: Column(
          children: [
            TextFormField(
              controller: nameCart,
              decoration: const InputDecoration(
                  label: Text('nombre Carrito'), border: OutlineInputBorder()),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Ingrese nombre Cart';
                }
                return null;
              },
              onSaved: (saved) =>
                  addCart(cart: Carts(status: 'pendiente', id: saved).toJson()),
            ),
            ElevatedButton.icon(
                onPressed: () {
                  if (form.currentState!.validate()) {
                    form.currentState!.save();
                  }
                },
                icon: const Icon(Icons.save),
                label: const Text('Guardar'))
          ],
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:shopping_cart/blocs/carts_bloc.dart';


import 'package:shopping_cart/class/data_base.dart';

import 'package:shopping_cart/data_model/products_carts.dart';
import 'package:shopping_cart/product/add_remove_product_widget.dart';

class ListProductsIntoCart extends StatelessWidget with DataBase {
  ListProductsIntoCart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartsBloc, CartsState>(
      builder: (context, state) {
        return state.nameCart!.isEmpty
            ? const SizedBox()
            : StreamBuilder<List<ProductsCarts>>(
                stream: getListProductsIntoCart(idCart: state.nameCart!),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Center(
                      child: Text(snapshot.error.toString()),
                    );
                  }
                  if (snapshot.hasData) {
                    return ListView.separated(
                        itemBuilder: (context, index) {
                          ProductsCarts products = snapshot.data![index];
                          print(products.quantity);
                          return ListTile(
                            title: Text(products.nombre!),
                            subtitle: Text(products.descripcion!.toString()),
                            trailing: AddRemoveProductWidget(
                              idCart: state.nameCart,
                              idProduct: products.id,
                              quantity: products.quantity,
                            ),
                          );
                        },
                        separatorBuilder: (context, index) => const Divider(),
                        itemCount: snapshot.data!.length);
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                });
      },
    );
  }
}

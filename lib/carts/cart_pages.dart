import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:shopping_cart/blocs/carts_bloc.dart';
import 'package:shopping_cart/carts/create_cart.dart';
import 'package:shopping_cart/carts/list_carts_widget.dart';
import 'package:shopping_cart/class/decoration_widget.dart';
import 'package:shopping_cart/controller/carts.dart';

class CartsPage extends StatelessWidget with DecorationWidget {
  CartsPage({Key? key}) : super(key: key);

  final CartsController cartsController = Get.put(CartsController());

  void showCartLayout() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista Shopping Cart'),
        backgroundColor: Colors.green,
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: IconButton(
                icon: const Icon(Icons.close, color: Colors.grey),
                //onPressed: () => cartsController.showCartLayout(),
                onPressed: () =>
                    BlocProvider.of<CartsBloc>(context).add(ShowCartLayout()),
              ),
            ),
          )
        ],
      ),
      body: Container(
        margin: marginContainerPage(),
        decoration: containerDecorationPage(color: Colors.green),
        child: Column(
          children: [
            Expanded(child: ListCartsWidget()),
            CreateCart(),
          ],
        ),
      ),
    );
  }
}

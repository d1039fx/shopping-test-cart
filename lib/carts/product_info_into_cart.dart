import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/class/data_base.dart';
import 'package:shopping_cart/controller/carts.dart';
import 'package:shopping_cart/data_model/products.dart';

class ProductInfoIntoCart extends StatelessWidget with DataBase {
  final String idProduct;

  final CartsController cartsController = Get.put(CartsController());

  ProductInfoIntoCart({Key? key, required this.idProduct}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Products>(
        stream: getDataProduct(idCartProduct: idProduct),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          }
          if (snapshot.hasData) {
            return ListTile(
              title: Text(snapshot.data!.nombre!),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}

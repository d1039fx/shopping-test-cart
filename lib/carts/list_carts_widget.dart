import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/blocs/carts_bloc.dart';
import 'package:shopping_cart/class/data_base.dart';
import 'package:shopping_cart/controller/carts.dart';
import 'package:shopping_cart/data_model/carts.dart';

class ListCartsWidget extends StatelessWidget with DataBase {
  ListCartsWidget({Key? key}) : super(key: key);
  final CartsController cartsController = Get.put(CartsController());

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Carts>>(
        stream: getDataCartsList(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('error: ${snapshot.error.toString()}'),
            );
          }
          if (snapshot.hasData) {
            return ListView.separated(
                itemBuilder: (context, index) {
                  Carts cartsData = snapshot.data![index];
                  return ListTile(
                    title: Text(cartsData.id!),
                    subtitle: Text(cartsData.status!),
                    onTap: () => BlocProvider.of<CartsBloc>(context)
                        .add(ShowProductIntoCart(name: cartsData.id)),
                    // onTap: () => cartsController.showProductIntoCart(
                    //     name: cartsData.id!),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: snapshot.data!.length);
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}

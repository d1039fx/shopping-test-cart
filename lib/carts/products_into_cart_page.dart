import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:shopping_cart/blocs/carts_bloc.dart';
import 'package:shopping_cart/carts/list_products_into_cart.dart';
import 'package:shopping_cart/class/decoration_widget.dart';
import 'package:shopping_cart/controller/carts.dart';

class ProductsIntoCartPage extends StatelessWidget with DecorationWidget {
  ProductsIntoCartPage({Key? key}) : super(key: key);

  final CartsController cartsController = Get.put(CartsController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: BlocBuilder<CartsBloc, CartsState>(
          builder: (context, state) {
            return Text(state.nameCart!);
          },
        ),
        //title: Text(cartsController.nameCart.value),
        actions: [
          TextButton.icon(
              onPressed: () {},
              icon: const Icon(Icons.list_alt, color: Colors.white),
              label: const Text(
                'Crear orden',
                style: TextStyle(color: Colors.white),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
              backgroundColor: Colors.white,
              child: BlocBuilder<CartsBloc, CartsState>(
                builder: (context, state) {
                  return IconButton(
                      onPressed: () => BlocProvider.of<CartsBloc>(context)
                          .add(ShowProductIntoCart(name: state.nameCart)),

                      // onPressed: () =>
                      //     cartsController.showProductIntoCart(
                      //         name: cartsController.nameCart.value),
                      icon: const Icon(
                        Icons.close,
                        color: Colors.grey,
                      ));
                },
              ),
            ),
          )
        ],
      ),
      body: Container(
          margin: marginContainerPage(),
          decoration: containerDecorationPage(
            color: Colors.blue,
          ),
          child: ListProductsIntoCart()),
    );
  }
}

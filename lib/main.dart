import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shopping_cart/blocs/carts_bloc.dart';
import 'package:shopping_cart/blocs/quantity_bloc.dart';
import 'home/home_page.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const ShoppingTestCartBloc());
}

class ShoppingTestCartBloc extends StatelessWidget {
  const ShoppingTestCartBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(providers: [
      BlocProvider<CartsBloc>(create: (_) => CartsBloc()),
      BlocProvider<QuantityBloc>(create: (_) => QuantityBloc()),
    ], child: const ShoppingTestCart());
  }
}


class ShoppingTestCart extends StatelessWidget {
  const ShoppingTestCart({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shopping Test Cart',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

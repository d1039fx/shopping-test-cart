import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:shopping_cart/blocs/quantity_bloc.dart';

import 'package:shopping_cart/class/data_base.dart';

class AddRemoveProductWidget extends StatefulWidget {
  const AddRemoveProductWidget(
      {Key? key, this.idCart, this.idProduct, this.quantity})
      : super(key: key);
  final String? idCart, idProduct;
  final int? quantity;

  @override
  State<AddRemoveProductWidget> createState() => _AddRemoveProductWidgetState();
}

class _AddRemoveProductWidgetState extends State<AddRemoveProductWidget>
    with DataBase {
  void remove() {
    int data = widget.quantity!;
    data--;
    BlocProvider.of<QuantityBloc>(context).add(QuantityRemoveProduct(
        quantity: data, idCart: widget.idCart, idProduct: widget.idProduct));
  }

  int? add() {
    int data = widget.quantity!;
    data++;
    BlocProvider.of<QuantityBloc>(context).add(QuantityAddProduct(
        quantity: data, idCart: widget.idCart, idProduct: widget.idProduct));
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 120,
      child: Row(
        children: [
          CircleAvatar(
            maxRadius: 15,
            minRadius: 10,
            backgroundColor: Colors.red,
            child: IconButton(
                padding: const EdgeInsets.all(0),
                onPressed: remove,
                icon: const Icon(
                  Icons.remove,
                  color: Colors.white,
                )),
          ),
          Expanded(child: Center(child: Text(widget.quantity.toString()))),
          CircleAvatar(
            maxRadius: 15,
            minRadius: 10,
            backgroundColor: Colors.green,
            child: IconButton(
                padding: const EdgeInsets.all(0),
                onPressed: add,
                icon: const Icon(
                  Icons.add,
                  color: Colors.white,
                )),
          ),
        ],
      ),
    );
  }
}

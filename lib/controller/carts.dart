import 'package:get/state_manager.dart';

class CartsController extends GetxController {
  RxBool showCarts = false.obs;
  RxBool showProductsIntoCart = false.obs;
  RxString nameCart = ''.obs;

  void showCartLayout() {
    if (showCarts.value) {
      showCarts.value = false;
      showProductsIntoCart.value = false;
    } else {
      showCarts.value = true;
    }
  }

  void showProductIntoCart({required String name}) {
    if (showProductsIntoCart.value) {
      showProductsIntoCart.value = false;
    } else {
      showProductsIntoCart.value = true;
      nameCart.value = name;
    }
  }
}

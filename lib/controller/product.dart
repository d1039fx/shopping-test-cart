import 'package:get/get.dart';

class ProductController extends GetxController {
  RxInt quantity = 0.obs;

  void addProduct(int data) {
    quantity.value = data;
    quantity++;
  }

  void removeProduct(int data) {
    if (quantity.value >= 1) {
      quantity.value = data;
      quantity.value--;
    } else {
      quantity.value = data;
      quantity.value = 0;
    }
  }
}

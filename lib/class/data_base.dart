import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shopping_cart/data_model/carts.dart';
import 'package:shopping_cart/data_model/products.dart';
import 'package:shopping_cart/data_model/products_carts.dart';

class DataBase {
  final String collectionProducts = 'products';
  final String collectionCarts = 'carts';

  //list Products
  Stream<List<Products>> getDataProductsList() {
    return FirebaseFirestore.instance
        .collection(collectionProducts)
        .snapshots()
        .map<List<Products>>((event) => event.docs
            .map<Products>((e) => Products.fromJson(e.data()))
            .toList());
  }

  //product info
  Stream<Products> getDataProduct({required String idCartProduct}) {
    return FirebaseFirestore.instance
        .collection(collectionProducts)
        .where('id', isEqualTo: idCartProduct)
        .snapshots()
        .map<Products>((event) => Products.fromJson(event.docs.single.data()));
  }

  //add carts
  Future<String> addCart({required Map<String, dynamic> cart}) {
    return FirebaseFirestore.instance
        .collection(collectionCarts)
        .doc(cart['id'])
        .set(cart)
        .then((value) => 'Carrito de compras creado')
        .catchError((error) => error.toString());
  } //list carts

  Stream<List<Carts>> getDataCartsList() {
    return FirebaseFirestore.instance
        .collection(collectionCarts)
        .snapshots()
        .map<List<Carts>>((event) =>
            event.docs.map<Carts>((e) => Carts.fromJson(e.data())).toList());
  }

  Future<String> addProductIntoCart(
          {required String idCart, required Map<String, dynamic> data}) =>
      FirebaseFirestore.instance
          .collection(collectionCarts)
          .doc(idCart)
          .collection('products_into_cart')
          .doc(data['id'])
          .set(data)
          .then((value) => 'Producto adherido a $idCart');

  //list products in carts
  Stream<List<ProductsCarts>> getListProductsIntoCart(
          {required String idCart}) =>
      FirebaseFirestore.instance
          .collection(collectionCarts)
          .doc(idCart)
          .collection('products_into_cart')
          .snapshots()
          .map<List<ProductsCarts>>((event) => event.docs
              .map<ProductsCarts>((e) => ProductsCarts.fromJson(e.data()))
              .toList());

  //get product into cart
  Stream<ProductsCarts> getOnlyProductIntoCart(
          {required String idCart, required String idProduct}) =>
      FirebaseFirestore.instance
          .collection(collectionCarts)
          .doc(idCart)
          .collection('products_into_cart')
          .where('id', isEqualTo: idProduct)
          .snapshots()
          .map((event) => ProductsCarts.fromJson(event.docs.single.data()));

  //update product into cart
  Future<String> updateProductIntoCart(
          {required String idCart,
          required String idProduct,
          required Map<String, dynamic> data}) =>
      FirebaseFirestore.instance
          .collection(collectionCarts)
          .doc(idCart)
          .collection('products_into_cart')
          .doc(idProduct)
          .update(data)
          .then((value) => 'Producto actualizado');

  //delete product into cart
  Future<String> deleteProductIntoCart(
          {required String idCart,
          required String idProduct}) =>
      FirebaseFirestore.instance
          .collection(collectionCarts)
          .doc(idCart)
          .collection('products_into_cart')
          .doc(idProduct)
          .delete()
          .then((value) => 'Producto borrado')
          .catchError((error) => error.toString());
}

import 'package:flutter/material.dart';

class DecorationWidget{
  EdgeInsetsGeometry marginContainerPage(){
    return const EdgeInsets.all(8);
  }
  BoxDecoration containerDecorationPage({Color? color}) => BoxDecoration(
      color: color!.withOpacity(0.05),
      border: Border.all(color: color.withOpacity(0.2)),
      borderRadius: BorderRadius.circular(20));
}
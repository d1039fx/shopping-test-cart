part of 'quantity_bloc.dart';

@immutable
abstract class QuantityEvent {}

class QuantityRemoveProduct extends QuantityEvent {
  final int? quantity;
  final String? idCart, idProduct;

  QuantityRemoveProduct({this.quantity, this.idCart, this.idProduct});
}

class QuantityAddProduct extends QuantityEvent {
  final int? quantity;
  final String? idCart, idProduct;

  QuantityAddProduct({this.quantity, this.idCart, this.idProduct});
}

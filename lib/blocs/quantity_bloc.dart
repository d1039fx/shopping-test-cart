import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:shopping_cart/class/data_base.dart';

part 'quantity_event.dart';
part 'quantity_state.dart';

class QuantityBloc extends Bloc<QuantityEvent, QuantityState> with DataBase {
  QuantityBloc()
      : super(const QuantityInitial(
          quantity: 0,
        )) {
    on<QuantityEvent>((event, emit) {
      if (event is QuantityRemoveProduct) {
        if (event.quantity! < 1) {
          deleteProductIntoCart(
                  idCart: event.idCart!, idProduct: event.idProduct!)
              .then((value) => print(value));
        } else {
          updateProductIntoCart(
              idCart: event.idCart!,
              idProduct: event.idProduct!,
              data: {'quantity': event.quantity}).then((value) => print(value));
        }

      }
      if (event is QuantityAddProduct) {
        updateProductIntoCart(
            idCart: event.idCart!,
            idProduct: event.idProduct!,
            data: {'quantity': event.quantity}).then((value) => print(value));

      }
    });
  }
}

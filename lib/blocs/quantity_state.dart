part of 'quantity_bloc.dart';

@immutable
abstract class QuantityState {
  final int? quantity;


  const QuantityState({this.quantity});
}

class QuantityInitial extends QuantityState {
  const QuantityInitial({int? quantity})
      : super(quantity: quantity);

}

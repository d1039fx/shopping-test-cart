import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'carts_event.dart';
part 'carts_state.dart';

class CartsBloc extends Bloc<CartsEvent, CartsState> {
  CartsBloc()
      : super(const CartsInitial(
            nameCart: '', showProductsIntoCart: false, showCarts: false)) {
    on<CartsEvent>((event, emit) {
      if (event is ShowCartLayout) {
        if (state.showCarts!) {
          emit(const CartsInitial(
              showCarts: false, nameCart: '', showProductsIntoCart: false));
        } else {
          emit(const CartsInitial(
              showCarts: true, nameCart: '', showProductsIntoCart: false));
        }
      }
      if (event is ShowProductIntoCart) {
        if (state.showProductsIntoCart!) {
          emit(const CartsInitial(
              showCarts: true, nameCart: '', showProductsIntoCart: false));
        } else {
          emit(CartsInitial(
              showCarts: true,
              nameCart: event.name,
              showProductsIntoCart: true));
        }
      }
    });
  }
}

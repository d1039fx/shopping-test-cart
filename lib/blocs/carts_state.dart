part of 'carts_bloc.dart';

@immutable
abstract class CartsState {
  final bool? showCarts, showProductsIntoCart;
  final String? nameCart;

  const CartsState(
      {this.showCarts,
      this.showProductsIntoCart,
      this.nameCart});
}

class CartsInitial extends CartsState {
  const CartsInitial(
      {bool? showCarts, bool? showProductsIntoCart, String? nameCart})
      : super(
            showCarts: showCarts,
            showProductsIntoCart: showProductsIntoCart,
            nameCart: nameCart);
}

// class ShowCarts extends CartsState {
//   const ShowCarts(
//       {bool? showCarts, bool? showProductsIntoCart, String? nameCart})
//       : super(
//             showCarts: showCarts,
//             showProductsIntoCart: showProductsIntoCart,
//             nameCart: nameCart);
// }
//
// class ShowProductsIntoCarts extends CartsState {
//   const ShowProductsIntoCarts(
//       {bool? showCarts, bool? showProductsIntoCart, String? nameCart})
//       : super(
//             showCarts: showCarts,
//             showProductsIntoCart: showProductsIntoCart,
//             nameCart: nameCart);
// }
//
// class NameCart extends CartsState {
//   const NameCart(
//       {bool? showCarts, bool? showProductsIntoCart, String? nameCart})
//       : super(
//             showCarts: showCarts,
//             showProductsIntoCart: showProductsIntoCart,
//             nameCart: nameCart);
// }

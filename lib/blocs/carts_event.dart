part of 'carts_bloc.dart';

@immutable
abstract class CartsEvent {}

class ShowCartLayout extends CartsEvent{}

class ShowProductIntoCart extends CartsEvent{
  final String? name;

  ShowProductIntoCart({this.name});
}


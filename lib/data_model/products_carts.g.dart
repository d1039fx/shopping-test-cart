// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products_carts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductsCarts _$ProductsCartsFromJson(Map<String, dynamic> json) =>
    ProductsCarts(
      sku: json['sku'] as String?,
      descripcion: json['descripcion'] as String?,
      id: json['id'] as String?,
      nombre: json['nombre'] as String?,
      quantity: json['quantity'] as int?,
    );

Map<String, dynamic> _$ProductsCartsToJson(ProductsCarts instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nombre': instance.nombre,
      'sku': instance.sku,
      'descripcion': instance.descripcion,
      'quantity': instance.quantity,
    };

import 'package:json_annotation/json_annotation.dart';

part 'products_carts.g.dart';

@JsonSerializable()
class ProductsCarts {
  final String? id, nombre, sku, descripcion;
  final int? quantity;

  ProductsCarts(
      {this.sku, this.descripcion, this.id, this.nombre, this.quantity});

  Map<String, dynamic> toJson() => _$ProductsCartsToJson(this);

  factory ProductsCarts.fromJson(Map<String, dynamic> data) =>
      _$ProductsCartsFromJson(data);
}

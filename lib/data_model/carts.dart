
import 'package:json_annotation/json_annotation.dart';

part 'carts.g.dart';

@JsonSerializable()
class Carts{
  final String? id, status;

  Carts({this.id, this.status});

  Map<String, dynamic> toJson() => _$CartsToJson(this);

  factory Carts.fromJson(Map<String, dynamic> data) => _$CartsFromJson(data);
}
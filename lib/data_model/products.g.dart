// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Products _$ProductsFromJson(Map<String, dynamic> json) => Products(
      id: json['id'] as String?,
      nombre: json['nombre'] as String?,
      sku: json['sku'] as String?,
      descripcion: json['descripcion'] as String?,
    );

Map<String, dynamic> _$ProductsToJson(Products instance) => <String, dynamic>{
      'id': instance.id,
      'nombre': instance.nombre,
      'sku': instance.sku,
      'descripcion': instance.descripcion,
    };

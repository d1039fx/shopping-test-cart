
import 'package:json_annotation/json_annotation.dart';

part 'products.g.dart';

@JsonSerializable()
class Products{
  final String? id, nombre, sku, descripcion;

  Products({this.id, this.nombre, this.sku, this.descripcion});

  Map<String, dynamic> toJson() => _$ProductsToJson(this);

  factory Products.fromJson(Map<String, dynamic> data) => _$ProductsFromJson(data);

}